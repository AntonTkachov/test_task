module Comparator
  class Files
    class << self
      def compare(path1, path2)
        [path1, path2].each { |arg| validate_argument(arg) }

        lines1 = File.open(path1, "r").each_line.to_a.map(&:chomp)
        lines2 = File.open(path2, "r").each_line.to_a.map(&:chomp)
        lines1_index = 0
        lines2_index = 0
        @result_row_number = 1

        changes = ::Diff::LCS.diff(lines1, lines2)

        changes.each do |sub_changes|
          deletion_changes = filter_changes_by_sing(sub_changes, "-")
          addition_changes = filter_changes_by_sing(sub_changes, "+")

          # Output unchanged data
          if deletion_changes.first
            deletion_start_index = deletion_changes.first.position
            unchanged_elements_number = deletion_start_index - lines1_index
          else
            addition_start_index = addition_changes.first.position
            unchanged_elements_number = addition_start_index - lines2_index
          end
          unchanged_elements_number.times do
            print_result_row("", lines1[lines1_index])
            lines1_index += 1
            lines2_index += 1
          end

          # Output changed data
          changes_number = [deletion_changes.count, addition_changes.count].max
          (0...changes_number).each do |change_index|
            if deletion_changes[change_index] && addition_changes[change_index]
              print_result_row("*", lines1[lines1_index], lines2[lines2_index])
              lines1_index += 1
              lines2_index += 1
            elsif deletion_changes[change_index]
              print_result_row("-", lines1[lines1_index])
              lines1_index += 1
            else
              print_result_row("+", lines2[lines2_index])
              lines2_index += 1
            end
          end
        end

        # Unchanged elements in the end of files
        lines1[lines1_index...lines1.count].each do |row|
          print_result_row("", row)
        end

        nil
      end

      private
      def validate_argument(arg)
        raise InputException.new("Arguments have to be an absolute path to file") if arg[0] != "/" || !arg.is_a?(String)
        raise InputException.new("All arguments have to contain path to existing File") unless File.exists?(arg)
      end

      def print_result_row(sign, first_value, second_value = nil)
        result_value = sign == "*" ? "#{first_value}|#{second_value}" : first_value.to_s
        puts "#{@result_row_number}\t#{sign}\t#{result_value}"
        @result_row_number += 1
      end

      def filter_changes_by_sing(changes, sign)
        changes.select{ |change| change.action == sign }
      end
    end
  end
end
