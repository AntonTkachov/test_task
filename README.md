# Comparator

Gem that serves as a comparator to get the difference between two files

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'comparator'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install comparator

## Usage

Comparator::Files.compare(path1, path2)
