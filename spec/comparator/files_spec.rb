require 'spec_helper'

describe Comparator::Files do
  it "properly print the difference between two files" do
    path1 = @fixture_path + "file1"
    path2 = @fixture_path + "file2"
    diff = File.open(@fixture_path + "diff_file1_file2", "r").each_line.to_a.join 
    expect{ Comparator::Files.compare(path1, path2) }.to output(diff).to_stdout
  end
end
