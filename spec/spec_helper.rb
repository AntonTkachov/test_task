require 'bundler/setup'
Bundler.setup

require 'comparator'

RSpec.configure do |config|
  config.before(:all) do
    @fixture_path = File.dirname(__FILE__) + "/fixtures/"
  end
end
